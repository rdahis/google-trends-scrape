#!/usr/bin/env python3

#---------------------------------------------------------#
# Modules
#---------------------------------------------------------#

from lib.pytrends.request import TrendReq
import time
from random import randint
import csv
import os, sys
import lib.filetype_operations as fo
from lib.parse_GT_query import *


#---------------------------------------------------------#
# Main
#---------------------------------------------------------#

root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
input_dir = os.path.join(root_dir,'input')
temp_dir = os.path.join(root_dir,'temp')
output_dir = os.path.join(root_dir,'output')

google_username = "protesttrendsproject@gmail.com"
google_password = "trendsscrape"

month_block_size = 3
month_overlap = 1
sleep_time_min = 40
sleep_time_max = 80


def main():
    
    # Cleans output and temp folders
    folders_to_clean = [output_dir, temp_dir]
    clean_folders_contents(folders_to_clean)

    print("Starting Google Trends Scrape job.")

    # connect to Google
    pytrend = TrendReq(google_username, google_password, custom_useragent='My Pytrends Script')

    # read input searches
    with open(os.path.join(input_dir,'search_input.csv'), "rU") as incsv:
        with open(os.path.join(output_dir,'search.csv'), "w") as outcsv:
            reader = csv.DictReader(incsv)
            fieldnames = ['search_id',
                          'execution_time',
                          'sleep_time',
                          'keywords',
                          'country',
                          'date_block',
                          'date_start',
                          'date_end',
                          'flag']
            writer = csv.DictWriter(outcsv,fieldnames,extrasaction='ignore')
            writer.writeheader()

            # loop over searches
            counter = 1
            for row in reader:
                
                for block, date_start, date_end in parse_GT_query(row,month_block_size,month_overlap):
                    start_time = time.clock()

                    # make request / trend
                    trend_payload = {'q': block['keywords'],
                                     'geo': block['country'],
                                     'date': block['date_block']}
                    try:
                        trend = pytrend.trend(trend_payload)
                        df = pytrend.trend(trend_payload, return_type='dataframe')
                        df.to_csv(os.path.join(temp_dir,str(counter)+'.csv'), encoding='utf-8')
                        print('Downloaded request ' + str(counter) + '.')
                        flag = 'Successful.'
                    except KeyError: # not precise error handling, but functional for now
                        print('Failed to download request ' + str(counter) + '.')
                        flag = 'Unsuccessful. Not enough search volume to show results.'
                        pass
                    except TypeError: # not precise error handling, but functional for now
                        print('Reached quota limit. Try again later.')
                        sys.exit(0)
                        
                    elapsed_time = (time.clock() - start_time)

                    # wait a random amount of time between requests to avoid bot detection
                    start_sleeptime = time.clock()
                    time.sleep(randint(sleep_time_min, sleep_time_max))
                    elapsed_sleeptime = (time.clock() - start_sleeptime)
                    
                    # populate output/search.csv
                    outrow = {'search_id': counter,
                              'execution_time': elapsed_time,
                              'sleep_time': elapsed_sleeptime,
                              'keywords': block['keywords'],
                              'country': block['country'],
                              'data_block': block['date_block'],
                              'date_start': date_start.strftime('%d/%m/%Y'),
                              'date_end': date_end.strftime('%d/%m/%Y'),
                              'flag': flag}
                    writer.writerow(outrow)
                    
                    counter += 1


def clean_folders_contents(list_folders):
    """
    Cleans all content (files and subfolders) inside each folder, except for .gitkeep files.
    Input: list of folder path strings.
    """
    import os, shutil

    for folder in list_folders:
        for file in os.listdir(folder):
            if file == '.gitkeep':
                pass
            file_path = os.path.join(folder, file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path): shutil.rmtree(file_path)
            except Exception as e:
                print(e)



if __name__ == "__main__":
    main()


