


#---------------------------------------------------------#
# Modules
#---------------------------------------------------------#

import csv
import os, sys
import lib.filetype_operations as fo
from datetime import date, datetime, timedelta as td


#---------------------------------------------------------#
# Main
#---------------------------------------------------------#

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
INPUT_DIR = os.path.join(ROOT_DIR,'input')
TEMP_DIR = os.path.join(ROOT_DIR,'temp')
OUTPUT_DIR = os.path.join(ROOT_DIR,'output')


def main():

    with open(os.path.join(OUTPUT_DIR,'search.csv'), 'r') as incsv:
        reader = csv.DictReader(incsv)
        with open(os.path.join(OUTPUT_DIR, 'search_data.csv'), 'w') as outcsv:
            fieldnames = ['search_id',
                          'date',
                          'svi']
            writer = csv.DictWriter(outcsv,fieldnames,extrasaction='ignore')
            writer.writeheader()

            for row in reader:
                try:
                    with open(os.path.join(TEMP_DIR,str(row['search_id'])+'.csv'), 'r') as f:
                        temp = csv.DictReader(f)
                        for row_temp in temp:
                            line = {'search_id': row['search_id'],
                                    'date': row_temp['Date']}
                            row_temp.pop('Date') # to deal with Google returning keyword as column name
                            line['svi'] = list(row_temp.values())[0]
                            writer.writerow(line)
                except Exception: # search_id's with no results
                    d1 = datetime.strptime(row['date_start'], '%d/%m/%Y')
                    d2 = datetime.strptime(row['date_end'], '%d/%m/%Y')

                    for day in datetime_range(d1, d2):
                        line = {'search_id': row['search_id'],
                                'date': day.strftime('%Y-%m-%d'),
                                'svi': '0.0'}
                        writer.writerow(line)


def datetime_range(start=None, end=None):
    span = end - start
    for i in range(span.days + 1):
        yield start + td(days=i)



if __name__ == '__main__':
    main()

