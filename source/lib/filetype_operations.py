#!/usr/bin/env python

def merge_lists(list1, list2):
    '''
    Merges two lists, without duplicates.
    '''
    in_first = set(list1)
    in_second = set(list2)
    in_second_but_not_in_first = in_second - in_first
    result = list1 + list(in_second_but_not_in_first)
    return result

def dict_from_lists(keys,values):
    '''
    Returns dictionary from two lists.
    '''
    return dict(zip(keys, values))

def merge_two_dicts(x, y):
    '''Given two dicts, merge them into a new dict as a shallow copy.'''
    z = x.copy()
    z.update(y)
    return z

def get_header_from_dicts(dict1,dict2):
    '''
    Returns list containing keys of merged dicts.
    '''
    z = dict1.copy()
    z.update(dict2)
    return z.keys()

def get_header_from_list_of_dicts(list1):
    '''
    Returns sorted list containing largest set of dict keys.
    Input: list of dicts.
    '''
    z = dict(list1[0])
    for d in list1[1:]:
        z.update(d)
    z = z.keys()
    z.sort()
    return z

def empty_dict_from_list(list1):
    z = dict((el,'') for el in list1)
    return z

def dict_from_containing_keys(dict1,list1):
    '''
    Returns dictionary as a merge between dict1 and list1 keys.
    If list1 contains keys not in dict1, is returns empty values.
    '''
    assert all (k in list1 for k in dict1.keys())
    z = dict((el,'') for el in list1)
    z.update(dict1)
    return z

