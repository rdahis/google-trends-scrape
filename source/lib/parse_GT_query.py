

def parse_GT_query(search_row, block_size, block_overlap):
    '''
    Parses search into Google Trends' format with time blocks.
    Input: Dict with Google Trends search (keywords, country, start date, end date)
            Example: search_row = {'keywords':'protest',
                                   'country':'AO',
                                   'date_start':'01/2011',
                                   'date_end':'06/2014'}
           Google Trends block size (in months)
           Block overlap (in months)
    Output: Dict generator, each element being a compatible query for Google Trends.
                Yields overlapping blocks with 1 month overlap.
    '''
    
    import datetime as DT

    start = DT.datetime.strptime(search_row['date_start'], '%m/%Y')
    end = last_day_of_month(DT.datetime.strptime(search_row['date_end'], '%m/%Y'))
    
    curr = start

    while curr < end:
        if add_months(curr, block_size) < end:
            month_block = curr.strftime('%m/%Y') + ' ' + str(block_size) + 'm'
        else: # builds last search_block if month count would pass end
            last_block = int(end.strftime('%m')) - int(curr.strftime('%m')) + 1
            month_block = curr.strftime('%m/%Y') + ' ' + str(last_block) + 'm'
        search_block = {
                        'keywords': search_row['keywords'],
                        'date_block': month_block,
                        'country': search_row['country']
                       }
        date_start = curr
        date_end = last_day_of_month(add_months(curr, block_size))
        yield search_block, date_start, date_end
        curr = add_months(curr, block_size - block_overlap)


def add_months(sourcedate, months):
    '''
    Returns sourcedate plus # months in datetime format.
    Source:
    http://stackoverflow.com/questions/4130922/how-to-increment-datetime
    -by-custom-months-in-python-without-using-library
    '''
    import datetime
    import calendar
    
    month = sourcedate.month - 1 + months
    year = int(sourcedate.year + month / 12 )
    month = month % 12 + 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    
    return datetime.datetime(year,month,day)

def last_day_of_month(any_day):
    import datetime
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)  # this will never fail
    return next_month - datetime.timedelta(days=next_month.day)
